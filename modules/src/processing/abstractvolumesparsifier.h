#ifndef CTL_ABSTRACTVOLUMESPARSIFIER_H
#define CTL_ABSTRACTVOLUMESPARSIFIER_H

#include "img/sparsevoxelvolume.h"

namespace CTL {

class AbstractVolumeSparsifier
{
    public:virtual SparseVoxelVolume sparsify(const VoxelVolume<float>& volume) = 0;

public:
    virtual ~AbstractVolumeSparsifier() = default;

protected:
    AbstractVolumeSparsifier() = default;
    AbstractVolumeSparsifier(const AbstractVolumeSparsifier&) = default;
    AbstractVolumeSparsifier(AbstractVolumeSparsifier&&) = default;
    AbstractVolumeSparsifier& operator=(const AbstractVolumeSparsifier&) = default;
    AbstractVolumeSparsifier& operator=(AbstractVolumeSparsifier&&) = default;
};

} // namespace CTL

#endif // CTL_ABSTRACTVOLUMESPARSIFIER_H
