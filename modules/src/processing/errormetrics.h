#ifndef CTL_ERRORMETRICS_H
#define CTL_ERRORMETRICS_H

#include <numeric>
#include <vector>
#include <QtMath>

namespace CTL {

template <typename T>
class Chunk2D;
template <typename T>
class VoxelVolume;

namespace imgproc {

class AbstractErrorMetric
{
    public:virtual double operator()(const float* first1, const float* last1,
                                     const float* first2) const = 0;

public:
    double operator()(const std::vector<float>& vec1,
                      const std::vector<float>& vec2) const;

    double operator()(const Chunk2D<float>& chunk1,
                      const Chunk2D<float>& chunk2) const;

    double operator()(const VoxelVolume<float>& vol1,
                      const VoxelVolume<float>& vol2) const;

    virtual ~AbstractErrorMetric() = default;

protected:
    AbstractErrorMetric() = default;
    AbstractErrorMetric(const AbstractErrorMetric&) = default;
    AbstractErrorMetric(AbstractErrorMetric&&) = default;
    AbstractErrorMetric& operator=(const AbstractErrorMetric&) = default;
    AbstractErrorMetric& operator=(AbstractErrorMetric&&) = default;

    template <typename InputIt>
    static size_t zeroCheckedDistance(InputIt begin, InputIt end);
};

class L1Norm : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();
};

class RelativeL1Norm : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();
};

class L2Norm : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();
};

class RelativeL2Norm : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();
};

class RMSE : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();
};

class RelativeRMSE : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();
};

class CorrelationError : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();
};

class CosineSimilarityError : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();
};

class GemanMcClure : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    GemanMcClure(const double parameter);

    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();

    double parameter() const;

private:
    double _parameter;
};

class NormalizedGemanMcClure : public AbstractErrorMetric
{
    public:double operator()(const float* first1, const float* last1,
                             const float* first2) const override;
public:
    NormalizedGemanMcClure(double parameter);

    template <typename InputIt1, typename InputIt2>
    double operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const;

    using AbstractErrorMetric::operator();

    double parameter() const;

private:
    double _parameter;
};

} // namespace imgproc

namespace metric {
    extern const ::CTL::imgproc::L1Norm L1;
    extern const ::CTL::imgproc::RelativeL1Norm rL1;
    extern const ::CTL::imgproc::L2Norm L2;
    extern const ::CTL::imgproc::RelativeL2Norm rL2;
    extern const ::CTL::imgproc::RMSE RMSE;
    extern const ::CTL::imgproc::RelativeRMSE rRMSE;
    extern const ::CTL::imgproc::CorrelationError corrErr;
    extern const ::CTL::imgproc::CosineSimilarityError cosSimErr;
    extern const ::CTL::imgproc::GemanMcClure GMCPreuhs;
    extern const ::CTL::imgproc::NormalizedGemanMcClure nGMCPreuhs;
} //namespace metric

// Template implementation
// =======================

namespace imgproc {

template <class InputIt1, class InputIt2>
double L1Norm::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    double ret = 0.0;
    while(first1 != last1)
    {
        ret += std::fabs(*first1 - *first2);
        ++first1;
        ++first2;
    }

    return ret;
}

template <class InputIt1, class InputIt2>
double RelativeL1Norm::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    double ret = 0.0;
    double ref = 0.0;
    while(first1 != last1)
    {
        ret += std::fabs(*first1 - *first2);
        ref += std::fabs(*first2);
        ++first1;
        ++first2;
    }
    if(qFuzzyIsNull(ref))
        qWarning("undefined relative L1 norm");

    return ret / ref;
}

template <class InputIt1, class InputIt2>
double L2Norm::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    double ret = 0.0;
    while(first1 != last1)
    {
        ret += std::pow(*first1 - *first2, 2.0);
        ++first1;
        ++first2;
    }

    return std::sqrt(ret);
}

template <class InputIt1, class InputIt2>
double RelativeL2Norm::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    double ret = 0.0;
    double ref = 0.0;
    while(first1 != last1)
    {
        ret += std::pow(*first1 - *first2, 2.0);
        ref += std::pow(*first2, 2.0);
        ++first1;
        ++first2;
    }
    if(qFuzzyIsNull(ref))
        qWarning("undefined relative L2 norm");

    return std::sqrt(ret) / std::sqrt(ref);
}

template <class InputIt1, class InputIt2>
double RMSE::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    const auto nbEl = zeroCheckedDistance(first1, last1);

    if(nbEl == 0) return 0.0;

    return L2Norm{}(first1, last1, first2) / std::sqrt(nbEl);
}

template <class InputIt1, class InputIt2>
double RelativeRMSE::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    return RelativeL2Norm{}(first1, last1, first2);
}

template <class InputIt1, class InputIt2>
double CorrelationError::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    const auto nbEl = zeroCheckedDistance(first1, last1);

    if(nbEl == 0) return 0.0;

    const auto mean1 = std::accumulate(first1, last1, 0.0) / static_cast<double>(nbEl);
    const auto mean2 = std::accumulate(first2, std::next(first2, nbEl), 0.0) / static_cast<double>(nbEl);

    double numer = 0.0;
    double denom1 = 0.0;
    double denom2 = 0.0;
    while(first1 != last1)
    {
        numer  += (*first1 - mean1) * (*first2 - mean2);
        denom1 += std::pow(*first1 - mean1, 2.0);
        denom2 += std::pow(*first2 - mean2, 2.0);
        ++first1;
        ++first2;
    }
    const auto resDenom = denom1 * denom2;
    if(qFuzzyIsNull(resDenom))
    {
        qWarning("undefined correlation");
        return 1.0;
    }

    return 1.0 - (numer / std::sqrt(resDenom));
}

template <class InputIt1, class InputIt2>
double CosineSimilarityError::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    double numer = 0.0;
    double denom1 = 0.0;
    double denom2 = 0.0;
    while(first1 != last1)
    {
        numer  += *first1 * *first2;
        denom1 += std::pow(*first1, 2.0);
        denom2 += std::pow(*first2, 2.0);
        ++first1;
        ++first2;
    }
    auto resDenom = denom1 * denom2;
    if(qFuzzyIsNull(resDenom))
    {
        qWarning("undefined cosine similarity");
        return 1.0;
    }

    return 1.0 - (numer / std::sqrt(resDenom));
}

template <class InputIt1, class InputIt2>
double GemanMcClure::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    const auto invPar = 1.0 / _parameter;

    double ret = 0.0;
    double squaredDiff;
    while(first1 != last1)
    {
        squaredDiff = std::pow(*first1 - *first2, 2.0);
        ret += squaredDiff / (1.0 + invPar * squaredDiff);
        ++first1;
        ++first2;
    }

    return ret;
}

template <class InputIt1, class InputIt2>
double NormalizedGemanMcClure::operator()(InputIt1 first1, InputIt1 last1, InputIt2 first2) const
{
    const auto nbEl = zeroCheckedDistance(first1, last1);

    if(nbEl == 0) return 0.0;

    return GemanMcClure{ _parameter }(first1, last1, first2)
        / (_parameter * static_cast<double>(nbEl));
}

template <typename InputIt>
size_t AbstractErrorMetric::zeroCheckedDistance(InputIt first, InputIt last)
{
    const auto nbEl = std::distance(first, last);

    if(nbEl == 0)
        qWarning("Tried to compute error metric from zero input elements.");

    return nbEl;
}

} // namespace imgproc
} // namespace CTL

#endif // CTL_ERRORMETRICS_H
