#ifndef CTL_THRESHOLDVOLUMESPARSIFIER_H
#define CTL_THRESHOLDVOLUMESPARSIFIER_H

#include "abstractvolumesparsifier.h"

namespace CTL {

class ThresholdVolumeSparsifier : public AbstractVolumeSparsifier
{
    public:virtual SparseVoxelVolume sparsify(const VoxelVolume<float>& volume) override;

public:
    ThresholdVolumeSparsifier(float threshold);

    void setThreshold(float threshold);
    float threshold() const;

private:
    float _thresh;

};

} // namespace CTL

#endif // CTL_THRESHOLDVOLUMESPARSIFIER_H
