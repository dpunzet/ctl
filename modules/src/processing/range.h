#ifndef CTL_RANGE_H
#define CTL_RANGE_H

#include <algorithm>
#include <vector>

typedef unsigned int uint;

namespace CTL {

// Range
template<typename T>
class Range
{
public:
    // ctor
    template<typename T1, typename T2,
             typename = typename std::enable_if<std::is_convertible<T1, T>::value &&
                                                std::is_convertible<T2, T>::value>::type>
    Range(T1 start, T2 end) : _data{ static_cast<T>(start), static_cast<T>(end) }
    { }

    static Range fromCenterAndWidth(T center, T width);

    // start and end of the range
    T& start();
    T& end();
    const T& start() const;
    const T& end() const;

    // center of the range
    T center() const;
    // width of the range
    T width() const;

    // spacing that a linspace vector would have, given a specific number of samples
    T spacing(uint nbSamples) const;

    // linspace
    std::vector<T> linspace(uint nbSamples) const;
    // static linspace
    static std::vector<T> linspace(T from, T to, uint nbSamples);

private:
    // data member (start and end)
    T _data[2];
};

typedef Range<uint>  IndexRange;
typedef Range<float> SamplingRange;

template<typename T>
Range<T> Range<T>::fromCenterAndWidth(T center, T width)
{
    return Range<T>(center - width / T(2), center + width / T(2));
}

template<typename T>
T Range<T>::width() const { return _data[1] - _data[0]; }

template<typename T>
T Range<T>::spacing(uint nbSamples) const
{
    return (nbSamples > 1) ? (_data[1] - _data[0]) / T(nbSamples - 1)
            : T(0);
}

template<typename T>
T& Range<T>::start() { return _data[0]; }

template<typename T>
T& Range<T>::end() { return _data[1]; }

template<typename T>
const T& Range<T>::start() const { return _data[0]; }

template<typename T>
const T& Range<T>::end() const { return _data[1]; }

template<typename T>
T Range<T>::center() const { return (_data[0] + _data[1]) / T(2); }

template<typename T>
std::vector<T> Range<T>::linspace(uint nbSamples) const
{
    return linspace(_data[0], _data[1], nbSamples);
}

template<typename T>
std::vector<T> Range<T>::linspace(T from, T to, uint nbSamples)
{
    std::vector<T> ret(nbSamples);
    const T increment = (nbSamples > 1) ? (to - from) / T(nbSamples - 1)
                                        : T(0);
    uint idx = 0;
    std::generate(ret.begin(), ret.end(), [&idx, from, increment]
                                          { return from + T(idx++) * increment; });

    return ret;
}

} // namespace CTL

#endif // CTL_RANGE_H
