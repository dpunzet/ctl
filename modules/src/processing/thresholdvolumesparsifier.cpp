#include "thresholdvolumesparsifier.h"
#include "img/voxelvolume.h"

namespace CTL {

ThresholdVolumeSparsifier::ThresholdVolumeSparsifier(float threshold)
    : _thresh(threshold)
{
}

void ThresholdVolumeSparsifier::setThreshold(float threshold)
{
    _thresh = threshold;
}

float ThresholdVolumeSparsifier::threshold() const
{
    return _thresh;
}

SparseVoxelVolume ThresholdVolumeSparsifier::sparsify(const VoxelVolume<float>& volume)
{
    const auto& offset = volume.offset();
    const auto& dim    = volume.dimensions();
    const auto voxSize = SparseVoxelVolume::VoxelSize{ volume.voxelSize().x,
                                                       volume.voxelSize().y,
                                                       volume.voxelSize().z };

    const std::array<float, 3> volCorner { offset.x - 0.5f * float(dim.x - 1) * voxSize.x,
                                           offset.y - 0.5f * float(dim.y - 1) * voxSize.y,
                                           offset.z - 0.5f * float(dim.z - 1) * voxSize.z };

    std::vector<SparseVoxelVolume::SingleVoxel> sparseList;
    for(uint z = 0; z < volume.dimensions().z; ++z)
        for(uint y = 0; y < volume.dimensions().y; ++y)
            for(uint x = 0; x < volume.dimensions().x; ++x)
            {
                const auto value = volume(x,y,z);
                if(value >= _thresh)
                {
                    SparseVoxelVolume::SingleVoxel vox{ static_cast<float>(x) * voxSize.x + volCorner[0],
                                                        static_cast<float>(y) * voxSize.y + volCorner[1],
                                                        static_cast<float>(z) * voxSize.z + volCorner[2],
                                                        value };
                    sparseList.push_back(vox);
                }
            }

    return SparseVoxelVolume(voxSize, std::move(sparseList));
}


} // namespace CTL
