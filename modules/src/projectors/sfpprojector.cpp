#include "sfpprojector.h"
#include "acquisition/geometryencoder.h"
#include "components/abstractdetector.h"
#include "img/sparsevoxelvolume.h"
#include "mat/deg.h"
#include "mat/pi.h"
#include "ocl/clfileloader.h"
#include "ocl/openclconfig.h"

#include <QDebug>

namespace CTL {
namespace OCL {

const std::string CL_FILE_NAME_TR = "projectors/projection_tr.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TT = "projectors/projection_tt.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TT_GEN = "projectors/projection_tt_generic.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TR_ATOM_FLT = "projectors/projection_tr_atomicFloat.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TT_ATOM_FLT = "projectors/projection_tt_atomicFloat.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TT_GEN_ATOM_FLT = "projectors/projection_tt_generic_atomicFloat.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TT_GEN_SPARSE = "projectors/projection_tt_generic_sparse.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TT_GEN_SPARSE_ATOM_FLT = "projectors/projection_tt_generic_sparse_atomicFloat.cl"; //!< path to .cl file
const std::string CL_KERNEL_NAME = "proj"; //!< name of the OpenCL kernel function
const std::string CL_PROGRAM_NAME_TR = "projector_tr"; //!< OCL program name for TR kernel
const std::string CL_PROGRAM_NAME_TT = "projector_tt"; //!< OCL program name for TT kernel
const std::string CL_PROGRAM_NAME_TT_GEN = "projector_tt_generic"; //!< OCL program name for TT_generic kernel
const std::string CL_PROGRAM_NAME_TT_GEN_SPARSE = "projector_tt_generic_sparse"; //!< OCL program name for sparse TT_generic kernel
const std::string CL_PROGRAM_NAME_TR_ATOM_FLT = "projector_tr_atomFlt"; //!< OCL program name for TR kernel (atomic float)
const std::string CL_PROGRAM_NAME_TT_ATOM_FLT = "projector_tt_atomFlt"; //!< OCL program name for TT kernel (atomic float)
const std::string CL_PROGRAM_NAME_TT_GEN_ATOM_FLT = "projector_tt_generic_atomFlt"; //!< OCL program name for TT_generic kernel (atomic float)
const std::string CL_PROGRAM_NAME_TT_GEN_SPARSE_ATOM_FLT = "projector_tt_generic_sparse_atomFlt"; //!< OCL program name for sparse TT_generic kernel (atomic float)

SFPProjector::SFPProjector()
{
    initOpenCL();
}

void SFPProjector::configure(const AcquisitionSetup& setup)
{
    qDebug() << "Configure SFPProjector";

    // get projection matrices
    _pMats = GeometryEncoder::encodeFullGeometry(setup);

    // extract required system geometry
    auto detectorPixels = setup.system()->detector()->nbPixelPerModule();
    _viewDim.nbRows = uint(detectorPixels.height());
    _viewDim.nbChannels = uint(detectorPixels.width());
    _viewDim.nbModules = setup.system()->detector()->nbDetectorModules();

    // determine angle corrections
    _angleCorrAzi = determineAzimutCorrection();
    _angleCorrPol = determineThetaCorrection();
}

ProjectionData SFPProjector::project(const VolumeData& volume)
{
    // the returned object
    ProjectionData ret(_viewDim);

    // check for a valid volume
    if(!volume.hasData())
    {
        qCritical() << "no or contradictory data in volume object";
        return ret;
    }
    if(volume.smallestVoxelSize() <= 0.0f)
        qWarning() << "voxel size is zero or negative";

    if(!qFuzzyCompare(volume.voxelSize().x, volume.voxelSize().y)) // x and y sizes not equal
        performAnisotropyCorrection( { volume.voxelSize().x,
                                       volume.voxelSize().y,
                                       volume.voxelSize().z } );

    // projection dimensions
    const auto nbSingleViews = _pMats.size();
    const auto pixelPerView  = _viewDim.totalNbElements();
    evaluatePmats();

    // volume specs
    cl::size_t<3> volDimCL;
    volDimCL[0] = volume.dimensions().x;
    volDimCL[1] = volume.dimensions().y;
    volDimCL[2] = volume.dimensions().z;

    // choose correct OpenCL program
    std::string oclProgramName = programName();

    try // exception handling
    {
        // check for valid OpenCLConfig
        auto& oclConfig = OpenCLConfig::instance();
        if(!oclConfig.isValid())
            throw std::runtime_error("OpenCLConfig has not been initiated");

        // context and device to use (use first device)
        const auto& context = oclConfig.context();
        const auto& device = oclConfig.devices().at(0);

        // Create kernel
        auto* kernel = oclConfig.kernel(CL_KERNEL_NAME, oclProgramName);
        if(kernel == nullptr)
            throw std::runtime_error("kernel pointer not valid");

        // Create command queue
        cl::CommandQueue queue(context, device);

        // Concatenate all pMats
        const auto pMatsVectorized = vectorizedPmats();

        // get parameter array and local buffer size
        const auto parArray = parameterArray(volume, device);
        const auto locBufferItems = localBufferToUse(device);

        // create buffers
        const auto projBufSize = nbSingleViews * pixelPerView * sizeof(float);
        const auto aziBufSize  = _angleCorrAzi.size() * sizeof(float);
        const auto polBufSize  = _angleCorrPol.size() * sizeof(float);
        const auto matBufSize  = pMatsVectorized.size() * sizeof(float);
        const auto parBufSize  = 16 * sizeof(float);
        qDebug() << "create buffers (buffer sizes:" << projBufSize << aziBufSize << polBufSize << matBufSize << parBufSize << ")";

        auto projBuf = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, projBufSize);
        auto aziBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, aziBufSize);
        auto polBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, polBufSize);
        auto matBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, matBufSize);
        auto parBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, parBufSize);
        auto volImg  = cl::Image3D(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                                   cl::ImageFormat(CL_INTENSITY, CL_FLOAT),
                                   volDimCL[0], volDimCL[1], volDimCL[2]);

        // write buffers and volume image
        qDebug() << "write buffers";
        queue.enqueueWriteBuffer(aziBuf, CL_FALSE, 0, aziBufSize, _angleCorrAzi.data(), nullptr);
        queue.enqueueWriteBuffer(polBuf, CL_FALSE, 0, polBufSize, _angleCorrPol.data(), nullptr);
        queue.enqueueWriteBuffer(matBuf, CL_FALSE, 0, matBufSize, pMatsVectorized.data(), nullptr);
        queue.enqueueWriteBuffer(parBuf, CL_FALSE, 0, parBufSize, parArray.data(), nullptr);
        queue.enqueueWriteImage(volImg, CL_FALSE, cl::size_t<3>(), volDimCL, 0, 0,
                                const_cast<float*>(volume.rawData()));

        // set kernel arguments
        qDebug() << "set kernel args";
        kernel->setArg(0, projBuf);
        kernel->setArg(1, aziBuf);
        kernel->setArg(2, polBuf);
        kernel->setArg(3, matBuf);
        kernel->setArg(4, parBuf);
        kernel->setArg(5, volImg);
        kernel->setArg(6, locBufferItems);
        kernel->setArg(7, locBufferItems * sizeof(int), nullptr);

        // start kernel
        const auto globalWorksize = cl::NDRange(xWorksize(volume, device),
                                                totalNbProjections(),
                                                nbReqAreas(locBufferItems));
        const auto localWorksize  = cl::NDRange(globalWorksize[0], 1, 1);

        qDebug() << "start kernel with size: " << globalWorksize[0] << globalWorksize[1] << globalWorksize[2];

        queue.enqueueNDRangeKernel(*kernel, cl::NullRange, globalWorksize, localWorksize);

        // read result
        qDebug() << "read results";
        auto resVec = std::vector<float>(nbSingleViews * pixelPerView);
        queue.enqueueReadBuffer(projBuf, CL_TRUE, 0, projBufSize, resVec.data());
        ret.setDataFromVector(resVec);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    } catch(const std::bad_alloc& except)
    {
        qCritical() << "Allocation error:" << except.what();
    } catch(const std::exception& except)
    {
        qCritical() << "std exception:" << except.what();
    }

    return ret;
}

ProjectionData SFPProjector::projectSparse(const SparseVoxelVolume& volume)
{
    // the returned object
    ProjectionData ret(_viewDim);

    // check for a valid volume
    if(volume.data().empty())
    {
        qCritical() << "no or contradictory data in volume object";
        return ret;
    }
    if(std::min( { volume.voxelSize().x, volume.voxelSize().y, volume.voxelSize().z } ) <= 0.0f)
        qWarning() << "voxel size is zero or negative";

    if(!qFuzzyCompare(volume.voxelSize().x, volume.voxelSize().y)) // x and y sizes not equal
        performAnisotropyCorrection( { volume.voxelSize().x,
                                       volume.voxelSize().y,
                                       volume.voxelSize().z } );

    // projection dimensions
    const auto nbSingleViews = _pMats.size();
    const auto pixelPerView  = _viewDim.totalNbElements();
    evaluatePmats();

    // volume specs
    cl::size_t<3> volDimCL;
    volDimCL[0] = volume.nbVoxels();
    volDimCL[1] = 1;
    volDimCL[2] = 1;

    // choose correct OpenCL program
    std::string oclProgramName = _settings.useAtomicFloat ? CL_PROGRAM_NAME_TT_GEN_SPARSE_ATOM_FLT
                                                          : CL_PROGRAM_NAME_TT_GEN_SPARSE;

    try // exception handling
    {
        // check for valid OpenCLConfig
        auto& oclConfig = OpenCLConfig::instance();
        if(!oclConfig.isValid())
            throw std::runtime_error("OpenCLConfig has not been initiated");

        // context and device to use (use first device)
        const auto& context = oclConfig.context();
        const auto& device = oclConfig.devices().at(0);

        // Create kernel
        auto* kernel = oclConfig.kernel(CL_KERNEL_NAME, oclProgramName);
        if(kernel == nullptr)
            throw std::runtime_error("kernel pointer not valid");

        // Create command queue
        cl::CommandQueue queue(context, device);

        // Concatenate all pMats
        const auto pMatsVectorized = vectorizedPmats();

        // get parameter array and local buffer size
        const auto parArray = parameterArray(volume, device);
        const auto locBufferItems = localBufferToUse(device);

        // create buffers
        const auto projBufSize = nbSingleViews * pixelPerView * sizeof(float);
        const auto aziBufSize  = _angleCorrAzi.size() * sizeof(float);
        const auto polBufSize  = _angleCorrPol.size() * sizeof(float);
        const auto matBufSize  = pMatsVectorized.size() * sizeof(float);
        const auto parBufSize  = 16 * sizeof(float);
        qDebug() << "create buffers (buffer sizes:" << projBufSize << aziBufSize << polBufSize << matBufSize << parBufSize << ")";

        auto projBuf = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, projBufSize);
        auto aziBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, aziBufSize);
        auto polBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, polBufSize);
        auto matBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, matBufSize);
        auto parBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, parBufSize);
        static_assert(sizeof(SparseVoxelVolume::SingleVoxel) == 4 * sizeof(float),
                "The size of SparseVoxelVolume::SingleVoxel must equal the size of 4 floats. "
                "If any compiler performs padding for this type, an appropriate implementation is "
                "required for copying the data to an OpenCL device.");
        auto volBuf  = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, 4 * volDimCL[0] * sizeof(float));
        auto volImg  = cl::Image1DBuffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                                         cl::ImageFormat(CL_RGBA, CL_FLOAT),
                                         volDimCL[0], volBuf);

        // write buffers and volume image
        qDebug() << "write buffers";
        queue.enqueueWriteBuffer(aziBuf, CL_FALSE, 0, aziBufSize, _angleCorrAzi.data(), nullptr);
        queue.enqueueWriteBuffer(polBuf, CL_FALSE, 0, polBufSize, _angleCorrPol.data(), nullptr);
        queue.enqueueWriteBuffer(matBuf, CL_FALSE, 0, matBufSize, pMatsVectorized.data(), nullptr);
        queue.enqueueWriteBuffer(parBuf, CL_FALSE, 0, parBufSize, parArray.data(), nullptr);
        queue.enqueueWriteImage(volImg, CL_FALSE, cl::size_t<3>(), volDimCL, 0, 0,
                                const_cast<SparseVoxelVolume::SingleVoxel*>(volume.data().data()));

        // set kernel arguments
        qDebug() << "set kernel args";
        kernel->setArg(0, projBuf);
        kernel->setArg(1, aziBuf);
        kernel->setArg(2, polBuf);
        kernel->setArg(3, matBuf);
        kernel->setArg(4, parBuf);
        kernel->setArg(5, volImg);
        kernel->setArg(6, locBufferItems);
        kernel->setArg(7, locBufferItems * sizeof(int), nullptr);

        // start kernel
        const auto globalWorksize = cl::NDRange(xWorksize(volume, device),
                                                totalNbProjections(),
                                                nbReqAreas(locBufferItems));
        const auto localWorksize  = cl::NDRange(globalWorksize[0], 1, 1);

        qDebug() << "start kernel with size: " << globalWorksize[0] << globalWorksize[1] << globalWorksize[2];

        queue.enqueueNDRangeKernel(*kernel, cl::NullRange, globalWorksize, localWorksize);

        // read result
        qDebug() << "read results";
        auto resVec = std::vector<float>(nbSingleViews * pixelPerView);
        queue.enqueueReadBuffer(projBuf, CL_TRUE, 0, projBufSize, resVec.data());
        ret.setDataFromVector(resVec);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    } catch(const std::bad_alloc& except)
    {
        qCritical() << "Allocation error:" << except.what();
    } catch(const std::exception& except)
    {
        qCritical() << "std exception:" << except.what();
    }

    return ret;
}

SFPProjector::Settings& SFPProjector::settings()
{
    return _settings;
}

void SFPProjector::initOpenCL()
{
    try // OCL exception catching
    {
        auto& oclConfig = OpenCLConfig::instance();
        // general checks
        if(!oclConfig.isValid())
            throw std::runtime_error("OpenCLConfig is not valid");

        // add required OpenCL kernels

        auto addKernel = [&oclConfig] (const std::string& clFileName, const std::string& clProgramName)
        {
            // load source code from file
            ClFileLoader clFile(clFileName);
            if(!clFile.isValid())
                throw std::runtime_error(clFileName + "\nis not readable");
            const auto clSourceCode = clFile.loadSourceCode();
            // add kernel to OCLConfig
            oclConfig.addKernel(CL_KERNEL_NAME, clSourceCode, clProgramName);
        };

        // ++ TR kernel +++
        addKernel(CL_FILE_NAME_TR, CL_PROGRAM_NAME_TR);
        // ++ TT kernel +++
        addKernel(CL_FILE_NAME_TT, CL_PROGRAM_NAME_TT);
        // ++ TT_generic kernel +++
        addKernel(CL_FILE_NAME_TT_GEN, CL_PROGRAM_NAME_TT_GEN);

        // ++ TR kernel - atomic float version +++
        addKernel(CL_FILE_NAME_TR_ATOM_FLT, CL_PROGRAM_NAME_TR_ATOM_FLT);
        // ++ TT kernel - atomic float version +++
        addKernel(CL_FILE_NAME_TT_ATOM_FLT, CL_PROGRAM_NAME_TT_ATOM_FLT);
        // ++ TT kernel - atomic float version +++
        addKernel(CL_FILE_NAME_TT_GEN_ATOM_FLT, CL_PROGRAM_NAME_TT_GEN_ATOM_FLT);


        // ++ sparse TT_generic kernel +++
        addKernel(CL_FILE_NAME_TT_GEN_SPARSE, CL_PROGRAM_NAME_TT_GEN_SPARSE);
        // ++ sparse TT_generic kernel - atomic float version +++
        addKernel(CL_FILE_NAME_TT_GEN_SPARSE_ATOM_FLT, CL_PROGRAM_NAME_TT_GEN_SPARSE_ATOM_FLT);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }

}

QVector<float> SFPProjector::determineAzimutCorrection() const
{
    const auto detWidth  = _viewDim.nbChannels;
    const auto nbViews   = _pMats.size();
    const auto nbModules = _viewDim.nbModules;

    QVector<float> angleCorrAzi(detWidth * nbViews * nbModules);

    uint proj = 0;
    for(uint view = 0; view < nbViews; ++view)
        for(uint mod = 0; mod < nbModules; ++mod)
        {
            for(uint s = 0; s < detWidth; ++s)   // s-dependent fan-correction of phi (by means of gamma)
            {
                const float ph = phi(s, _pMats.at(view).at(mod));//beta-gamma(s);
                angleCorrAzi[s + detWidth * proj] = 1.0f / std::max(std::abs(std::cos(ph)), std::abs(std::sin(ph)));
            }
            ++proj;
        }

    return angleCorrAzi;
}

QVector<float> SFPProjector::determineThetaCorrection() const
{
    const auto detHeight = _viewDim.nbRows;
    const auto nbViews   = _pMats.size();
    const auto nbModules = _viewDim.nbModules;

    QVector<float> angleCorrPol(detHeight * nbViews * nbModules);

    uint proj = 0;
    for(uint view = 0; view < nbViews; ++view)
        for(uint mod = 0; mod < nbModules; ++mod)
        {
            for(uint t = 0; t < detHeight; ++t)
            {
                const float th = theta(t, _pMats.at(view).at(mod));
                angleCorrPol[t + detHeight * proj] = 1.0f / std::max(std::abs(std::cos(th)), std::abs(std::sin(th)));
            }
            ++proj;
        }

    return angleCorrPol;
}

float SFPProjector::phi(uint s, const mat::ProjectionMatrix& pMat) const
{
    const auto direction = pMat.directionSourceToPixel(double(s), std::round((_viewDim.nbRows - 1u) / 2.0));
    return static_cast<float>(std::atan2(direction.get<1>(), direction.get<0>()));
}

float SFPProjector::theta(uint t, const mat::ProjectionMatrix& pMat) const
{
    const auto direction = pMat.directionSourceToPixel(std::round((_viewDim.nbChannels - 1u) / 2.0), double(t));
    return static_cast<float>(std::atan(direction.get<2>() /
                                        std::sqrt(std::pow(direction.get<1>(), 2) +
                                                  std::pow(direction.get<0>(), 2))));
}

void SFPProjector::evaluatePmats() const
{
    static const auto maxTwist      = std::sin(0.1_deg);
    static const auto maxTilt       = std::sin(1.0_deg);
    static const auto maxTwistGenTT = std::sin(30.0_deg);

    for(const auto& pView : _pMats)
        for(const auto& pMod : pView)
        {
            const auto R = pMod.rotationMatR();

            const auto twist = std::abs(R(0,2));

            if(_settings.footprintType == TT_Generic)
            {
                if (twist > maxTwistGenTT)
                {
                    qWarning() << "Potential inaccuracy: setup contains a view with a detector "
                                  "twist larger than 30.0 deg. Consider using a different projector.";
                    return;
                }
            }
            else
            {
                if (twist > maxTwist)
                {
                    qWarning() << "Potential inaccuracy: setup contains a view with a detector "
                                  "twist larger than 0.1 deg. Consider using TT_Generic footprint"
                                  "for more accurate results.";
                    return;
                }
                const auto tilt  = std::abs(R(2,2));
                if (tilt > maxTilt)
                {
                    qWarning() << "Potential inaccuracy: setup contains a view with a detector "
                                  "tilt larger than 1.0 deg. Consider using TT_Generic footprint"
                                  "for more accurate results.";
                    return;
                }
            }
        }
}

/*!
 * Returns number of integers to be used in local buffer. Always a multiple of the number of
 * columns in a detector module. Restricted to 90% of available local memory on \a device.
 */
int SFPProjector::localBufferToUse(const cl::Device& device) const
{
    static constexpr auto maxLocMemFrac = 0.9f;
    const auto locMemSize = maxLocMemFrac * static_cast<float>(device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>());

    auto maxNbFullRows = static_cast<int>(locMemSize / sizeof(float) / _viewDim.nbChannels);

    qDebug() << "Used local mem items:" << maxNbFullRows * _viewDim.nbChannels
             << "(avail.:" << locMemSize / sizeof(float) << ")";

    if (maxNbFullRows == 0)
        throw std::domain_error("Not enough local memory available on OpenCL device to cover at "
                                "least one full detector row.");

    return maxNbFullRows * _viewDim.nbChannels;
}

size_t SFPProjector::maxUsedWorksize(const cl::Device &device)
{
    return device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() / 2;
}

int SFPProjector::nbReqAreas(int localBufferItems) const
{
    const auto nbFullRowsPerGroup = static_cast<float>(localBufferItems / _viewDim.nbChannels);
    return static_cast<int>(std::ceil(float(_viewDim.nbRows) / nbFullRowsPerGroup));
}

std::array<float, 16> SFPProjector::parameterArray(const VolumeData& volume,
                                                   const cl::Device& device) const
{
    std::array<float, 16> par;
    par[0]  = volume.dimensions().x;
    par[1]  = volume.dimensions().y;
    par[2]  = volume.dimensions().z;

    par[3]  = volume.voxelSize().x;
    par[4]  = volume.voxelSize().y;
    par[5]  = volume.voxelSize().z;

    par[6]  = volume.offset().x - 0.5f * (volume.dimensions().x - 1) * volume.voxelSize().x;
    par[7]  = volume.offset().y - 0.5f * (volume.dimensions().y - 1) * volume.voxelSize().y;
    par[8]  = volume.offset().z - 0.5f * (volume.dimensions().z - 1) * volume.voxelSize().z;

    par[9]  = _viewDim.nbChannels;
    par[10] = _viewDim.nbRows;
    par[11] = static_cast<float>(totalNbProjections());

    par[12] = 0.0f;
    par[13] = 0.0f;
    par[14] = 1.0f;

    const auto xPerWorker = volume.dimensions().x / maxUsedWorksize(device);
    par[15] = static_cast<float>(xPerWorker);

    return par;
}

std::array<float, 16> SFPProjector::parameterArray(const SparseVoxelVolume& volume, const cl::Device& device) const
{
    std::array<float, 16> par;
    par[0]  = static_cast<float>(volume.nbVoxels());
    par[1]  = 1.0f;
    par[2]  = 1.0f;

    par[3]  = volume.voxelSize().x;
    par[4]  = volume.voxelSize().y;
    par[5]  = volume.voxelSize().z;

    par[6]  = 0.0f;
    par[7]  = 0.0f;
    par[8]  = 0.0f;

    par[9]  = static_cast<float>(_viewDim.nbChannels);
    par[10] = static_cast<float>(_viewDim.nbRows);
    par[11] = static_cast<float>(totalNbProjections());

    par[12] = 0.0f;
    par[13] = 0.0f;
    par[14] = 1.0f;

    const auto voxPerWorker = volume.nbVoxels() / maxUsedWorksize(device);
    par[15] = static_cast<float>(voxPerWorker);

    return par;
}

void SFPProjector::performAnisotropyCorrection(mat::Matrix<3,1> voxelSize)
{
    const auto detWidth  = _viewDim.nbChannels;
    const auto nbViews   = _pMats.size();
    const auto nbModules = _viewDim.nbModules;

    uint proj = 0;
    for(uint view = 0; view < nbViews; ++view)
        for(uint mod = 0; mod < nbModules; ++mod)
        {
            for(uint s = 0; s < detWidth; ++s)
            {
                const float ph = phi(s, _pMats.at(view).at(mod));//beta-gamma(s);
                const auto inv_sin_abs = std::abs(std::sin(ph)) / float(voxelSize.get<1>());
                const auto inv_cos_abs = std::abs(std::cos(ph)) / float(voxelSize.get<0>());
                const auto anisotropyScaling = std::max(inv_sin_abs, inv_cos_abs);

                _angleCorrAzi[s + detWidth * proj] = 1.0f / (anisotropyScaling * float(voxelSize.get<0>()));
            }

            ++proj;
        }

}

std::string SFPProjector::programName() const
{
    std::string ret;
    switch (_settings.footprintType) {
    case CTL::OCL::SFPProjector::TR:
        ret = _settings.useAtomicFloat ? CL_PROGRAM_NAME_TR_ATOM_FLT
                                       : CL_PROGRAM_NAME_TR;
        break;
    case CTL::OCL::SFPProjector::TT:
        ret = _settings.useAtomicFloat ? CL_PROGRAM_NAME_TT_ATOM_FLT
                                       : CL_PROGRAM_NAME_TT;
        break;
    case CTL::OCL::SFPProjector::TT_Generic:
        ret = _settings.useAtomicFloat ? CL_PROGRAM_NAME_TT_GEN_ATOM_FLT
                                       : CL_PROGRAM_NAME_TT_GEN;
        break;
    }

    return ret;
}

uint SFPProjector::totalNbProjections() const
{
    return _pMats.size() * _viewDim.nbModules;
}

QVector<float> SFPProjector::vectorizedPmats() const
{
    QVector<float> ret;
    ret.reserve(totalNbProjections() * 16);

    static const QVector<float> zeroVec(4, 0.0f);
    for(const auto& viewPMat : _pMats)      // all views
        for(const auto& modPMat : viewPMat) // all modules
        {
            QVector<float> tmp(12);
            std::copy(modPMat.constBegin(), modPMat.constEnd(), tmp.begin());
            ret.append(tmp);
            ret.append(zeroVec); // fill with 4 zeros for cl_float16 usage in kernel
        }

    return ret;
}

size_t SFPProjector::xWorksize(const VolumeData& volume, const cl::Device& device)
{
    const auto maxWIPG = maxUsedWorksize(device);
    const auto allXFit = volume.dimensions().x <= maxWIPG;

    return allXFit ? volume.dimensions().x : maxWIPG;
}

size_t SFPProjector::xWorksize(const SparseVoxelVolume& volume, const cl::Device& device)
{
    const auto maxWIPG      = maxUsedWorksize(device);
    const auto allVoxelsFit = volume.nbVoxels() <= maxWIPG;

    return allVoxelsFit ? volume.nbVoxels() : maxWIPG;
}

} // namespace OCL
} // namespace CTL
