#include "messagehandler.h"
#include <QDateTime>
#include <QFile>
#include <QTextStream>

namespace CTL {

MessageHandler::MessageHandler() : QObject(nullptr)
{
}

bool MessageHandler::isBlacklisted(QtMsgType type, const QMessageLogContext& context) const
{
    return _blacklistAll ||
            isBlacklistedMessageType(type) ||
            isBlacklistedClassOrFct(context) ||
            isBlacklistedFile(context);
}

bool MessageHandler::isBlacklistedClassOrFct(const QMessageLogContext& context) const
{
    return screenList(_blacklistClassFct, context.function);
}

bool MessageHandler::isBlacklistedFile(const QMessageLogContext& context) const
{
    return screenList(_blacklistFiles, context.file);
}

bool MessageHandler::isBlacklistedMessageType(QtMsgType type) const
{
    return _blacklistMsgType[type];
}

bool MessageHandler::isWhitelisted(const QMessageLogContext& context) const
{
    return isWhitelistedClassOrFct(context) || isWhitelistedFile(context);
}

bool MessageHandler::isWhitelistedClassOrFct(const QMessageLogContext& context) const
{
    return screenList(_whitelistClassFct, context.function);
}

bool MessageHandler::isWhitelistedFile(const QMessageLogContext& context) const
{
    return screenList(_whitelistFiles, context.file);
}

/*!
 * Returns a reference to the singleton instance.
 *
 * Use this method whenever you want to interact with the message handler.
 */
MessageHandler& MessageHandler::instance()
{
    static MessageHandler theInstance;

    return theInstance;
}

void MessageHandler::processMessage(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    // whitelisted messages ignore blacklists
    const auto blacklisted = isWhitelisted(context) ? false : isBlacklisted(type, context);

    if(blacklisted && !_logBlacklistedMsg)
        return;

    constexpr int dateTagSize{12}, timeTagSize{15}, typeTagSize{10}, maxMsgSize{256};

    QString logString;
    logString.reserve(msg.size() + _showDateTag * dateTagSize
                                 + _showTimeTag * timeTagSize
                                 + _showTypeTag * typeTagSize
                                 + _showMsgOrig * maxMsgSize);

    if(_showTimeTag) logString += dateTimeTag();
    if(_showTypeTag) logString += typeTag(type);

    // append actual message
    logString += msg;

    if(_showMsgOrig) logString += messageOriginString(context);

    // append the current message to the full log
    _theLog.append(logString);
    emit newLogEntry();

    // print message if appropriate
    if(!blacklisted && !_squelched)
        printMessage(logString, type);

    // terminate for fatal messages
    if(type == QtFatalMsg) abort();
}

/*!
 * Returns the entire log as a QStringList (each list entry refers to a single message).
 *
 * Note that if logging of blacklisted messages is enabled (see enforceLoggingOfBlacklistMsg()),
 * this also contains all blacklisted messages.
 */
const QStringList& MessageHandler::log() const { return _theLog; }

/*!
 * Returns the last message as a QString.
 *
 * Note that if logging of blacklisted messages is enabled (see enforceLoggingOfBlacklistMsg()),
 * this can also return a blacklisted message (ie. a message that has not been printed to console
 * output).
 */
QString MessageHandler::lastMessage() const
{
    return _theLog.isEmpty() ? QStringLiteral("") : _theLog.last();
}

/*!
 * Blacklists all messages. Can be used in combination with whitelisting to filter individual
 * messages.
 *
 * Note that the effect of this cannot be reverted using clearAllBlacklists(), as no actual
 * blacklist entries are created. Instead, call blacklistAll(false) to disable full blacklisting.
 *
 * Example: suppress all message output except for messages from the CarmGantry class
 * \code
 * // blacklist everything
 * MessageHandler::instance().blacklistAll();
 *
 * // whitelist CarmGantry
 * MessageHandler::instance().whitelistClassOrFunction("CarmGantry");
 * \endcode
 *
 * \sa whitelistClassOrFunction(), whitelistFile().
 */
void MessageHandler::blacklistAll(bool blacklist)
{
    _blacklistAll = blacklist;
}

/*!
 * Blacklists all messages of type \a type.
 *
 * The following types are available:
 * - QtDebugMsg
 * - QtInfoMsg
 * - QtWarningMsg
 * - QtCriticalMsg
 * - QtFatalMsg
 */
void MessageHandler::blacklistMessageType(QtMsgType type)
{
    if(type < 5)
        _blacklistMsgType[type] = true;
}

/*!
 * Removes all entries from the blacklists.
 *
 * Note that this does not revert the effect of blacklistAll().
 */
void MessageHandler::clearAllBlacklists()
{
    _blacklistClassFct.clear();
    _blacklistFiles.clear();
    std::fill_n(_blacklistMsgType, 5, false);
}

/*!
 * Removes all entries from the whitelists.
 */
void MessageHandler::clearAllWhitelists()
{
    _whitelistFiles.clear();
    _whitelistClassFct.clear();
}

/*!
 * Enforces that messages from blacklisted origins will still be added to the log. Thus, they will
 * be written to the log file and can also be retrieved using log() and lastMessage(), if required.
 */
void MessageHandler::enforceLoggingOfBlacklistMsg(bool enabled)
{
    _logBlacklistedMsg = enabled;
}

/*!
 * Sets the file name used for saving the log file to \a fileName.
 *
 * Note that the file needs to be writable in order to successfully create the log file.
 */
void MessageHandler::setLogFileName(const QString& fileName)
{
    _logfileName = fileName;
}

/*!
 * Sets use of quiet mode to \a enabled. In quiet mode, no messages are printed to console output.
 */
void MessageHandler::setQuiet(bool enabled) { _squelched = enabled; }

/*!
 * Sets use of quiet mode to \a enabled. In quiet mode, no messages are printed to console output.
 *
 * Same as setQuiet().
 */
void MessageHandler::squelch(bool enabled) { _squelched = enabled; }

/*!
 * Sets the inclusion of the date tag into message strings to \a show.
 */
void MessageHandler::toggleDateTag(bool show) { _showDateTag = show; }

/*!
 * Sets the inclusion of the time tag into message strings to \a show.
 */
void MessageHandler::toggleTimeTag(bool show) { _showTimeTag = show; }

/*!
 * Sets the inclusion of the type tag into message strings to \a show.
 */
void MessageHandler::toggleTypeTag(bool show) { _showTypeTag = show; }

/*!
 * Sets the inclusion of the message origin tag into message strings to \a show.
 */
void MessageHandler::toggleMessageOriginTag(bool show)
{
    _showMsgOrig = show;
}

/*!
 * Sets the inclusion of all message tags into message strings to \a show.
 *
 * \sa toggleDateTag(), toggleTimeTag(), toggleTypeTag(), toggleMessageOriginTag().
 */
void MessageHandler::toggleAllTags(bool show)
{
    toggleDateTag(show);
    toggleMessageOriginTag(show);
    toggleTimeTag(show);
    toggleTypeTag(show);
}

/*!
 * Initiates writing of the log file to the file name specified by setLogFileName().
 *
 * Returns \c true on a successful write.
 */
bool MessageHandler::writeLogFile() const
{
    QFile file(_logfileName);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qWarning("Could not write log file. Failed to open file: %s", _logfileName.toLocal8Bit().constData());
        return false;
    }

    QTextStream out(&file);
    out << _theLog.join("\n");

    return out.status() == QTextStream::Ok;
}

/*!
 * Same as writeLogFile() but also clears the current log after a successful write.
 */
bool MessageHandler::writeLogFileAndClear()
{
    auto writeSuccess = writeLogFile();

    if(writeSuccess)
        _theLog.clear();

    return writeSuccess;
}

/*!
 * Slot that can be used to connect signals from external classes that emit a QString to process
 * it as a QtInfoMsg.
 */
void MessageHandler::messageFromSignal(QString msg)
{
    processMessage(QtInfoMsg, QMessageLogContext(), msg);
}

/*!
 * Function that needs to be passed to qInstallMessageHandler() in order to let this message
 * handler taking care of the Qt message streams.
 *
 * Usage:
 * \code
 * qInstallMessageHandler(MessageHandler::qInstaller);
 * \endcode
 */
void MessageHandler::qInstaller(QtMsgType type, const QMessageLogContext& context, const QString &msg)
{
    instance().processMessage(type, context, msg);
}

QString MessageHandler::typeTag(QtMsgType type)
{
    switch (type) {
    case QtDebugMsg:
        return QStringLiteral("Debug: ");
    case QtInfoMsg:
        return QStringLiteral("Info: ");
    case QtWarningMsg:
        return QStringLiteral("Warning: ");
    case QtCriticalMsg:
        return QStringLiteral("Critical: ");
    case QtFatalMsg:
        return QStringLiteral("Fatal: ");
    }

    return QStringLiteral("Unknown: ");
}

QString MessageHandler::dateTimeTag() const
{
    QString format = _showDateTag ? QStringLiteral("MM-dd-yyyy ") : QStringLiteral("");
    if(_showTimeTag) format += QStringLiteral("hh:mm:ss.zzz");

    return QStringLiteral("[") + QDateTime::currentDateTime().toString(format) + QStringLiteral("] ");
}

QString MessageHandler::messageOriginString(const QMessageLogContext& context)
{
    QString ret;
    ret.reserve(256);
    if(context.function) ret += QStringLiteral(" | ") + QString(context.function);
    if(context.file)     ret += QStringLiteral("; ") + QString(context.file) + QStringLiteral(":") + QString::number(context.line);

    return ret;
}

void MessageHandler::printMessage(const QString& finalMsg, QtMsgType type) const
{
    FILE* stream = stderr;
    if(type == QtDebugMsg || type == QtInfoMsg)
        stream = stdout;

    fprintf(stream, "%s\n", finalMsg.toLocal8Bit().constData());
    fflush(stream);

    emit messagePrinted();
}

bool MessageHandler::screenList(const QStringList& list, const char* str)
{
    if(!str)
        return false;

    QString inspectedString(str);
    for(const auto& entry : list)
        if(inspectedString.contains(entry))
            return true;

    return false;
}

} // namespace CTL
