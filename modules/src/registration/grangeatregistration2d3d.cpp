#include "grangeatregistration2d3d.h"
#include "io/printnloptmsg.h"
#include "processing/consistency.h"

namespace CTL {
namespace NLOPT {

namespace {

struct DataForOptimization
{
    const std::shared_ptr<const std::vector<float>>& projIntermedFct;
    const OCL::VolumeResampler& volumeIntermedResampler;
    const OCL::Radon3DCoordTransform& radon3DCoordTransform;
    const imgproc::AbstractErrorMetric& metric;
};

float computeDeltaS(const OCL::VolumeResampler& volIntermedFct, const mat::ProjectionMatrix& pMat);
double objFct(const std::vector<double>& x, std::vector<double>& grad, void* data);
Matrix3x3 rotationMatrix(const Vector3x1& axis);

} // unnamed namespace

mat::Homography3D GrangeatRegistration2D3D::optimize(const Chunk2D<float>& projectionImage,
                                                     const OCL::VolumeResampler& volumeIntermedResampler,
                                                     const mat::ProjectionMatrix& pMat)
{
    // calculate s spacing in sinogram
    const auto deltaS = computeDeltaS(volumeIntermedResampler, pMat);
    Q_ASSERT(deltaS > 0.0f);

    // initialize IntermedGen2D3D
    OCL::IntermedGen2D3D gen;
    gen.setLineDistance(deltaS);
    if(_subSamplingLevel != 1.0f)
        gen.setSubsampleLevel(_subSamplingLevel);

    // calculate initial intermediate functions
    auto initalIntermedFctPair = gen.intermedFctPair(projectionImage, pMat, volumeIntermedResampler);
    qDebug() << "initial inconsistency:" << initalIntermedFctPair.inconsistency(*_metric);

    // initialize transformation of 3d Radon coordinates
    const OCL::Radon3DCoordTransform transf(gen.lastSampling());

    // initialize optimization
    DataForOptimization data{ initalIntermedFctPair.ptrToFirst(), volumeIntermedResampler, transf,
                              *_metric };
    _opt.set_min_objective(objFct, &data);

    // perform optimization
    std::vector<double> param(6, 0.0);
    double remainInconsistency;
    auto errCode = _opt.optimize(param, remainInconsistency);

    // output messages
    qDebug() << "remaining inconsistency:" << remainInconsistency;
    printNloptMsg(errCode);

    return Homography3D(rotationMatrix({ param[0], param[1], param[2] }), // rotation matrix
                        Vector3x1{ param[3], param[4], param[5] });       // translation vector
}

nlopt::opt& GrangeatRegistration2D3D::optObject() { return _opt; }

const imgproc::AbstractErrorMetric* GrangeatRegistration2D3D::metric() const { return _metric; }

void GrangeatRegistration2D3D::setMetric(const imgproc::AbstractErrorMetric* metric)
{
    if(metric == nullptr)
        qWarning("A nullptr to AbstractErrorMetric is ignored.");
    else
        _metric = metric;
}

float GrangeatRegistration2D3D::subSamplingLevel() const { return _subSamplingLevel; }

void GrangeatRegistration2D3D::setSubSamplingLevel(float subSamplingLevel)
{
    _subSamplingLevel = subSamplingLevel;
}

namespace {

float computeDeltaS(const OCL::VolumeResampler& volIntermedFct, const mat::ProjectionMatrix& pMat)
{
    Q_ASSERT(volIntermedFct.volDim().z > 1);
    const auto& dRange = volIntermedFct.rangeDim3();
    const auto deltaD = (dRange.end() - dRange.start()) / (volIntermedFct.volDim().z - 1);
    const auto magnification = float(pMat.magnificationX() + pMat.magnificationY()) * 0.5f;
    return magnification * deltaD;
}

double objFct(const std::vector<double>& x, std::vector<double>&, void* data)
{
    // x = [r_ t_] = [rx ry rz tx ty tz]
    const auto* d = static_cast<DataForOptimization*>(data);
    const auto H = Homography3D(rotationMatrix({ x[0], x[1], x[2] }), { x[3], x[4], x[5] });
    const auto& transformedBuf = d->radon3DCoordTransform.transform(H);
    const IntermediateFctPair intermPair(d->projIntermedFct,
                                         d->volumeIntermedResampler.sample(transformedBuf),
                                         IntermediateFctPair::VolumeDomain);
    return intermPair.inconsistency(d->metric);
}

Matrix3x3 rotationMatrix(const Vector3x1& axis)
{
    // optimization uses deg internally for rotational degrees of freedom (since "deg ~ mm")
    return mat::rotationMatrix(axis * (PI / 180.0));
}

} // unnamed namespace

} // namespace NLOPT
} // namespace CTL
