#ifndef CTL_PROJECTIONREGISTRATION2D3D_H
#define CTL_PROJECTIONREGISTRATION2D3D_H

#include "abstractregistration2d3d.h"
#include "processing/errormetrics.h"
#include <nlopt.hpp>

namespace CTL {
namespace NLOPT {

/*!
 * \class ProjectionRegistration2D3D
 *
 * \brief DRR-based 2D/3D registration using NLopt for optimization
 */

class ProjectionRegistration2D3D : public AbstractNloptRegistration2D3D
{
public:
    ProjectionRegistration2D3D();

    mat::Homography3D optimize(const Chunk2D<float>& projectionImage,
                               const OCL::VolumeResampler& volume,
                               const mat::ProjectionMatrix& pMat) override;

    const imgproc::AbstractErrorMetric* metric() const override;
    void setMetric(const imgproc::AbstractErrorMetric* metric) override;

    float subSamplingLevel() const override;
    void setSubSamplingLevel(float subSamplingLevel) override;

    nlopt::opt& optObject() override;

private:
    nlopt::opt _opt{ nlopt::algorithm::LN_SBPLX, 6u };
    const imgproc::AbstractErrorMetric* _metric = &metric::L2;
};

} // namespace NLOPT
} // namespace CTL

#endif // CTL_PROJECTIONREGISTRATION2D3D_H
