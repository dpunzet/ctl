#include "projectionregistration2d3d.h"
#include "io/printnloptmsg.h"
#include "mat/homography.h"
#include "mat/matrix_algorithm.h"
#include "mat/matrix_utils.h"
#include "mat/pi.h"
#include "mat/projectionmatrix.h"
#include "ocl/openclconfig.h"
#include "ocl/clfileloader.h"
#include "ocl/pinnedmem.h"
#include "processing/volumeresampler.h"

const std::string CL_FILE_NAME_INTERP = "projectors/raycasterprojector_interp.cl"; //!< path to .cl file
const std::string CL_KERNEL_NAME = "ray_caster"; //!< name of the OpenCL kernel function
const std::string CL_PROGRAM_NAME_INTERP = "rayCaster_interp"; //!< OCL program name for interpolating kernel

namespace CTL {
namespace NLOPT {

namespace {

struct DataForOptimization
{
    const mat::ProjectionMatrix& P;
    const cl::CommandQueue& q;
    const cl::Buffer& sourceBuf;
    const cl::Buffer& qrBuf;
    const OCL::PinnedBufHostRead<float>& projBuf;
    const cl::Kernel& kernel;
    const imgproc::AbstractErrorMetric& metric;
    const Chunk2D<float>& projImg;
    std::vector<float> drr;
};

cl_float3 determineSource(const mat::ProjectionMatrix& P);

cl_float3 volumeCorner(const OCL::VolumeResampler& vol);

cl_float3 volumeVoxelSize(const OCL::VolumeResampler& vol);

cl_double16 decomposeM(const mat::Matrix<3, 3>& M);

double objFct(const std::vector<double>& x, std::vector<double>&, void* data);

Matrix3x3 rotationMatrix(const Vector3x1& axis);

} // unnamed namespace


ProjectionRegistration2D3D::ProjectionRegistration2D3D()
{
    OCL::OpenCLConfig::instance().addKernel(CL_KERNEL_NAME,
                                            OCL::ClFileLoader(CL_FILE_NAME_INTERP).loadSourceCode(),
                                            CL_PROGRAM_NAME_INTERP);
}

mat::Homography3D
ProjectionRegistration2D3D::optimize(const Chunk2D<float>& projectionImage,
                                     const OCL::VolumeResampler& volume,
                                     const mat::ProjectionMatrix& pMat)
{
    const auto& ctx = OCL::OpenCLConfig::instance().context();
    const cl::CommandQueue q{ ctx, OCL::OpenCLConfig::instance().devices().front() };

    // prepare OCL buffers
    const cl_uint2 raysPerPixel { { 1u, 1u } };
    const cl::Buffer raysPerPixelBuf(ctx, CL_MEM_READ_ONLY, sizeof raysPerPixel);
    q.enqueueWriteBuffer(raysPerPixelBuf, CL_FALSE, 0, sizeof raysPerPixel, &raysPerPixel);

    const auto volCorner = volumeCorner(volume);
    const cl::Buffer volCornerBuf(ctx, CL_MEM_READ_ONLY, sizeof volCorner);
    q.enqueueWriteBuffer(volCornerBuf, CL_FALSE, 0, sizeof volCorner, &volCorner);

    const auto voxSize = volumeVoxelSize(volume);
    const cl::Buffer voxSizeBuf(ctx, CL_MEM_READ_ONLY, sizeof voxSize);
    q.enqueueWriteBuffer(voxSizeBuf, CL_FALSE, 0, sizeof voxSize, &voxSize);

    const auto nbProjPixels = projectionImage.nbElements();
    const OCL::PinnedBufHostRead<float> projBuf(nbProjPixels, q);

    const cl::Buffer sourceBuf(ctx, CL_MEM_READ_ONLY, sizeof(cl_float3));
    const cl::Buffer qrBuf(ctx, CL_MEM_READ_ONLY, sizeof(cl_double16));

    // set fix kernel args
    auto kernel = OCL::OpenCLConfig::instance().kernel(CL_KERNEL_NAME, CL_PROGRAM_NAME_INTERP);
    kernel->setArg(0, 0.3f);
    kernel->setArg(1, raysPerPixelBuf);
    kernel->setArg(2, sourceBuf);
    kernel->setArg(3, volCornerBuf);
    kernel->setArg(4, voxSizeBuf);
    kernel->setArg(5, qrBuf);
    kernel->setArg(6, projBuf.devBuffer());
    kernel->setArg(7, volume.oclVolume());

    // set up optimization
    DataForOptimization data{ pMat, q, sourceBuf, qrBuf, projBuf, *kernel, *_metric,
                              projectionImage, std::vector<float>(projectionImage.nbElements()) };
    _opt.set_min_objective(objFct, &data);

    // perform optimization
    std::vector<double> param(6, 0.0);

    qDebug() << "initial inconsistency:" << objFct(param, param, &data);

    double remainInconsistency;
    const auto errCode = _opt.optimize(param, remainInconsistency);

    // output messages
    qDebug() << "remaining inconsistency:" << remainInconsistency;
    printNloptMsg(errCode);

    return mat::Homography3D(rotationMatrix({ param[0], param[1], param[2] }), // rotation matrix
                             Vector3x1{ param[3], param[4], param[5] });       // translation vector
}

const imgproc::AbstractErrorMetric* ProjectionRegistration2D3D::metric() const { return _metric; }

void ProjectionRegistration2D3D::setMetric(const imgproc::AbstractErrorMetric* metric)
{
    _metric = metric;
}

float ProjectionRegistration2D3D::subSamplingLevel() const { return 1.0f; }

void ProjectionRegistration2D3D::setSubSamplingLevel(float)
{
    qWarning() << "ProjectionRegistration2D3D::setSubSamplingLevel: "
                  "sub-sampling for DRR-based registration is not implemented yet.";
}

nlopt::opt& ProjectionRegistration2D3D::optObject() { return _opt; }

namespace {

cl_float3 determineSource(const mat::ProjectionMatrix& P)
{
    auto ret = P.sourcePosition();
    return { { static_cast<float>(ret.get<0>()), static_cast<float>(ret.get<1>()),
               static_cast<float>(ret.get<2>()) } };
}

cl_float3 volumeCorner(const OCL::VolumeResampler& vol)
{
   return { { vol.volOffset().x - 0.5f * vol.volDim().x * vol.volVoxSize().x,
              vol.volOffset().y - 0.5f * vol.volDim().y * vol.volVoxSize().y,
              vol.volOffset().z - 0.5f * vol.volDim().z * vol.volVoxSize().z } };
}

cl_float3 volumeVoxelSize(const OCL::VolumeResampler& vol)
{
    return { { vol.volVoxSize().x,
               vol.volVoxSize().y,
               vol.volVoxSize().z } };
}

cl_double16 decomposeM(const mat::Matrix<3, 3>& M)
{
    auto QR = mat::QRdecomposition(M);
    auto& Q = QR.Q;
    auto& R = QR.R;
    if(std::signbit(R(0, 0) * R(1, 1) * R(2, 2)))
        R = -R;
    cl_double16 ret = { { Q(0,0), Q(0,1), Q(0,2),
                          Q(1,0), Q(1,1), Q(1,2),
                          Q(2,0), Q(2,1), Q(2,2),
                          R(0,0), R(0,1), R(0,2),
                                  R(1,1), R(1,2),
                                          R(2,2) } };
    return ret;

}

double objFct(const std::vector<double>& x, std::vector<double>&, void* data)
{
    const auto d = static_cast<DataForOptimization*>(data);

    // apply geometry manipulation
    const auto H = mat::Homography3D(rotationMatrix({ x[0], x[1], x[2] }),
                                     { x[3], x[4], x[5] });
    const mat::ProjectionMatrix P = d->P * H;

    // compute new source position
    const auto source = determineSource(P);
    d->q.enqueueWriteBuffer(d->sourceBuf, CL_FALSE, 0, sizeof source, &source);

    // compute new QR decomposition
    const auto qr = decomposeM(P.M());
    d->q.enqueueWriteBuffer(d->qrBuf, CL_FALSE, 0, sizeof qr, &qr);

    // run kernel
    d->q.enqueueNDRangeKernel(d->kernel, cl::NullRange,
                              cl::NDRange{ d->projImg.dimensions().width,
                                           d->projImg.dimensions().height });

    // read DRR result
    d->projBuf.readFromDev(d->drr.data());

    // compute metric
    return d->metric(d->projImg.data(), d->drr);
}

Matrix3x3 rotationMatrix(const Vector3x1& axis)
{
    // optimization uses deg internally for rotational degrees of freedom (since "deg ~ mm")
    return mat::rotationMatrix(axis * (PI / 180.0));
}

} // unnamed namespace

} // namespace NLOPT
} // namespace CTL
