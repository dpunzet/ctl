#ifndef CTL_PROJECTIONDATA_H
#define CTL_PROJECTIONDATA_H

#include "singleviewdata.h"

namespace CTL {

/*!
 * \class ProjectionData
 *
 * \brief The ProjectionData class is the container class used to store all projections from
 * all views.
 *
 * This container holds all projection data in a vector of SingleViewData objects. Individual single
 * views can be added using append(). Alternatively, the entire data can be set from a std::vector
 * using setDataFromVector(). This might be useful to convert data from other sources that provide
 * the projection as a one dimensional memory block.
 */
class ProjectionData
{
private:
    template<class DataType, class NestedIteratorType>
    class PixelIterator;

public:
    struct Dimensions;
    typedef PixelIterator<ProjectionData, SingleViewData::iterator> iterator;
    typedef PixelIterator<const ProjectionData, SingleViewData::const_iterator> const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    explicit ProjectionData(const SingleViewData::Dimensions& viewDimensions);
    ProjectionData(uint channelsPerModule, uint rowsPerModule, uint nbModules);
    ProjectionData(const SingleViewData& singleViewData);
    ProjectionData(SingleViewData&& singleViewData);

    // pixel iterators
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    // getter methods
    const std::vector<SingleViewData>& constData() const;
    const std::vector<SingleViewData>& data() const;
    std::vector<SingleViewData>& data();
    Dimensions dimensions() const;
    const SingleViewData& first() const;
    SingleViewData& first();
    uint nbViews() const;
    SingleViewData& view(uint i);
    const SingleViewData& view(uint i) const;
    SingleViewData::Dimensions viewDimensions() const;

    // other methods
    void allocateMemory(uint nbViews);
    void allocateMemory(uint nbViews, float initValue);
    void append(SingleViewData&& singleView);
    void append(const SingleViewData& singleView);
    ProjectionData combined(const ModuleLayout& layout = ModuleLayout()) const;
    void fill(float fillValue);
    void freeMemory();
    float max() const;
    float min() const;
    void setDataFromVector(const std::vector<float>& dataVector);
    std::vector<float> toVector() const;
    void transformToExtinction(double i0 = 1.0);
    void transformToExtinction(const std::vector<double>& viewDependentI0);
    void transformToIntensity(double i0 = 1.0);
    void transformToIntensity(const std::vector<double>& viewDependentI0);
    void transformToCounts(double n0 = 1.0);
    void transformToCounts(const std::vector<double>& viewDependentN0);

    bool operator==(const ProjectionData& other) const;
    bool operator!=(const ProjectionData& other) const;

    // arithmetic operations
    ProjectionData& operator+=(const ProjectionData& other);
    ProjectionData& operator-=(const ProjectionData& other);
    ProjectionData& operator*=(float factor);
    ProjectionData& operator/=(float divisor);

    ProjectionData operator+(const ProjectionData& other) const;
    ProjectionData operator-(const ProjectionData& other) const;
    ProjectionData operator*(float factor) const;
    ProjectionData operator/(float divisor) const;

protected:
    SingleViewData::Dimensions _viewDim; //!< The dimensions of the individual single views.

    std::vector<SingleViewData> _data; //!< The internal data storage vector.

private:
    bool hasEqualSizeAs(const SingleViewData& other) const;
    template <class Function>
    void parallelExecution(const Function& f) const;
};

/*!
 * \struct ProjectionData::Dimensions
 * \brief Struct that holds the dimensions of a ProjectionData object.
 *
 * This contains the number of views (\c nbViews), the number of modules in each view (\c nbModules)
 * and the dimensions of individual modules, namely module width (\c nbChannels) and module height.
 */
struct ProjectionData::Dimensions
{
    uint nbChannels; //!< Number of channels in each module.
    uint nbRows;     //!< Number of rows in each module.
    uint nbModules;  //!< Number of modules.
    uint nbViews;    //!< Number of views.

    bool operator==(const Dimensions& other) const;
    bool operator!=(const Dimensions& other) const;

    std::string info() const;
    size_t totalNbElements() const;
};

// pixel iterator
template<class DataType, class NestedIteratorType>
class ProjectionData::PixelIterator
{
public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type        = typename NestedIteratorType::value_type;
    using difference_type   = std::ptrdiff_t;
    using pointer           = typename NestedIteratorType::pointer;
    using reference         = typename NestedIteratorType::reference;

    PixelIterator(DataType* projData = nullptr, uint view = 0, uint module = 0, uint pixel = 0);

    friend bool operator==(const PixelIterator& left, const PixelIterator& right)
    { return left._viewIterator == right._viewIterator; }
    friend bool operator!=(const PixelIterator& left, const PixelIterator& right)
    { return left._viewIterator != right._viewIterator; }

    PixelIterator&  operator++();
    PixelIterator   operator++(int);
    PixelIterator&  operator--();
    PixelIterator   operator--(int);

    reference       operator*() { return *_viewIterator; }
    const reference operator*() const { return *_viewIterator; }
    pointer         operator->() { return _viewIterator.operator->(); }

    template<class DataType2, class NestedIteratorType2>
    operator PixelIterator<DataType2, NestedIteratorType2>() const;

    uint module() const;
    uint view() const;

private:
    DataType* _dataPtr;
    NestedIteratorType _viewIterator;

    uint _curViewIdx{0};

    void nextView();
    void prevView();
};

template<class DataType, class NestedIteratorType>
ProjectionData::PixelIterator<DataType, NestedIteratorType>::PixelIterator(DataType* projData,
                                                                           uint view,
                                                                           uint module,
                                                                           uint pixel)
    : _dataPtr(projData)
    , _curViewIdx(view)
{
    _dataPtr = projData;

    if(_dataPtr)
        _viewIterator = NestedIteratorType(&_dataPtr->view(view), module, pixel);
}

template<class DataType, class NestedIteratorType>
ProjectionData::PixelIterator<DataType, NestedIteratorType>&
ProjectionData::PixelIterator<DataType, NestedIteratorType>::operator++()
{
    ++_viewIterator;
    if(_viewIterator == _dataPtr->view(_curViewIdx).end())
        nextView();

    return (*this);
}

template<class DataType, class NestedIteratorType>
ProjectionData::PixelIterator<DataType, NestedIteratorType>
ProjectionData::PixelIterator<DataType, NestedIteratorType>::operator++(int)
{
    auto temp(*this);
    ++(*this);
    return temp;
}

template<class DataType, class NestedIteratorType>
ProjectionData::PixelIterator<DataType, NestedIteratorType>&
ProjectionData::PixelIterator<DataType, NestedIteratorType>::operator--()
{
    if(_viewIterator == _dataPtr->view(_curViewIdx).begin())
        prevView();

    --_viewIterator;

    return (*this);
}

template<class DataType, class NestedIteratorType>
ProjectionData::PixelIterator<DataType, NestedIteratorType>
ProjectionData::PixelIterator<DataType, NestedIteratorType>::operator--(int)
{
    auto temp(*this);
    --(*this);
    return temp;
}

template<class DataType, class NestedIteratorType>
uint ProjectionData::PixelIterator<DataType, NestedIteratorType>::module() const
{
    return _viewIterator.module();
}

template<class DataType, class NestedIteratorType>
uint ProjectionData::PixelIterator<DataType, NestedIteratorType>::view() const
{
    return _curViewIdx;
}

template<class DataType, class NestedIteratorType>
void ProjectionData::PixelIterator<DataType, NestedIteratorType>::nextView()
{
    ++_curViewIdx;

    if(_curViewIdx < _dataPtr->nbViews())
        _viewIterator = NestedIteratorType(&_dataPtr->view(_curViewIdx), 0, 0);
    else
        --_curViewIdx;
}

template<class DataType, class NestedIteratorType>
void ProjectionData::PixelIterator<DataType, NestedIteratorType>::prevView()
{
    --_curViewIdx;

    if(_curViewIdx >= 0)
        _viewIterator = _dataPtr->view(_curViewIdx).end();
    else
        ++_curViewIdx;
}

} // namespace CTL

/*! \file */

#endif // CTL_PROJECTIONDATA_H
