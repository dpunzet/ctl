#ifndef CTL_SINGLEVIEWDATA_H
#define CTL_SINGLEVIEWDATA_H

#include "chunk2d.h"
#include "modulelayout.h"

namespace CTL {

/*!
 * \class SingleViewData
 *
 * \brief The SingleViewData class is the container class used to store all projections from
 * one particular view.
 *
 * In the generalized case, the detector consists of several individual flat-panel modules. Each of
 * the modules acquires one projection image. The full set of these images are stored in a
 * SingleViewData object.
 */
class SingleViewData
{
private:
    template<class DataType, class NestedIteratorType>
    class PixelIterator;

public:
    typedef Chunk2D<float> ModuleData; //!< Alias for template specialization Chunk2D<float>.
    struct Dimensions;

    typedef PixelIterator<SingleViewData, std::vector<float>::iterator> iterator;
    typedef PixelIterator<const SingleViewData, std::vector<float>::const_iterator> const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    // ctors
    explicit SingleViewData(const ModuleData::Dimensions& moduleDimensions);
    SingleViewData(uint channelsPerModule, uint rowsPerModule);
    SingleViewData(const ModuleData& moduleData);
    SingleViewData(ModuleData&& moduleData);

    // pixel iterators
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    // getter methods
    const std::vector<ModuleData>& constData() const;
    const std::vector<ModuleData>& data() const;
    std::vector<ModuleData>& data();
    Dimensions dimensions() const;
    uint elementsPerModule() const;
    const ModuleData& first() const;
    ModuleData& first();
    ModuleData& module(uint i);
    const ModuleData& module(uint i) const;
    uint nbModules() const;
    size_t totalPixelCount() const;

    // other methods
    void allocateMemory(uint nbModules);
    void allocateMemory(uint nbModules, float initValue);
    void append(ModuleData&& moduleData);
    void append(const ModuleData& moduleData);
    void append(std::vector<float>&& dataVector);
    void append(const std::vector<float>& dataVector);
    Chunk2D<float> combined(const ModuleLayout& layout = ModuleLayout(), bool* ok = nullptr) const;
    void fill(float fillValue);
    void freeMemory();
    float max() const;
    float min() const;
    void setDataFromVector(const std::vector<float>& dataVector);
    std::vector<float> toVector() const;
    void transformToExtinction(double i0orN0 = 1.0);
    void transformToIntensity(double i0 = 1.0);
    void transformToCounts(double n0 = 1.0);

    bool operator==(const SingleViewData& other) const;
    bool operator!=(const SingleViewData& other) const;

    SingleViewData& operator+=(const SingleViewData& other);
    SingleViewData& operator-=(const SingleViewData& other);
    SingleViewData& operator*=(float factor);
    SingleViewData& operator/=(float divisor);

    SingleViewData operator+(const SingleViewData& other) const;
    SingleViewData operator-(const SingleViewData& other) const;
    SingleViewData operator*(float factor) const;
    SingleViewData operator/(float divisor) const;

protected:
    ModuleData::Dimensions _moduleDim; //!< The dimensions of the individual modules.

    std::vector<ModuleData> _data;  //!< The internal data storage vector.

private:
    bool hasEqualSizeAs(const ModuleData& other) const;
    bool hasEqualSizeAs(const std::vector<float>& other) const;
};

/*!
 * \struct SingleViewData::Dimensions
 * \brief Struct that holds the dimensions, namely number of channels, rows and modules, of a
 * SingleViewData object.
 */
struct SingleViewData::Dimensions
{
    uint nbChannels; //!< Number of channels in each module.
    uint nbRows;     //!< Number of rows in each module.
    uint nbModules;  //!< Number of modules.

    bool operator==(const Dimensions& other) const;
    bool operator!=(const Dimensions& other) const;

    std::string info() const;
    size_t totalNbElements() const;
};

template<class DataType, class NestedIteratorType>
class SingleViewData::PixelIterator
{
public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type        = typename NestedIteratorType::value_type;
    using difference_type   = std::ptrdiff_t;
    using pointer           = typename NestedIteratorType::pointer;
    using reference         = typename NestedIteratorType::reference;

    PixelIterator(DataType* viewData = nullptr, uint module = 0, uint pixel = 0);

    friend bool operator==(const PixelIterator& left, const PixelIterator& right)
    { return left._moduleIterator == right._moduleIterator; }
    friend bool operator!=(const PixelIterator& left, const PixelIterator& right)
    { return left._moduleIterator != right._moduleIterator; }

    PixelIterator&  operator++();
    PixelIterator   operator++(int);
    PixelIterator&  operator--();
    PixelIterator   operator--(int);

    reference       operator*() {return *_moduleIterator;}
    const reference operator*() const {return *_moduleIterator;}
    pointer         operator->() {return _moduleIterator.operator->();}

    template<class DataType2, class NestedIteratorType2>
    operator PixelIterator<DataType2, NestedIteratorType2>() const;

    uint module() const;
    uint pixel() const;

private:
    DataType* _dataPtr;
    NestedIteratorType _moduleIterator;

    uint _curModule{0};

    void nextModule();
    void prevModule();
};


template<class DataType, class NestedIteratorType>
SingleViewData::PixelIterator<DataType, NestedIteratorType>::PixelIterator(DataType *viewData,
                                                                           uint module,
                                                                           uint pixel)
    : _dataPtr(viewData)
    , _curModule(module)
{
    _dataPtr = viewData;

    if(_dataPtr)
        _moduleIterator = _dataPtr->module(module).data().begin() + pixel;

}

template<class DataType, class NestedIteratorType>
SingleViewData::PixelIterator<DataType, NestedIteratorType>&
SingleViewData::PixelIterator<DataType, NestedIteratorType>::operator++()
{
    ++_moduleIterator;
    if(_moduleIterator == _dataPtr->module(_curModule).data().end())
        nextModule();

    return (*this);
}

template<class DataType, class NestedIteratorType>
SingleViewData::PixelIterator<DataType, NestedIteratorType>
SingleViewData::PixelIterator<DataType, NestedIteratorType>::operator++(int)
{
    auto temp(*this);
    ++(*this);
    return temp;
}

template<class DataType, class NestedIteratorType>
CTL::SingleViewData::PixelIterator<DataType, NestedIteratorType>&
SingleViewData::PixelIterator<DataType, NestedIteratorType>::operator--()
{
    if(_moduleIterator == _dataPtr->module(_curModule).data().begin())
        prevModule();

    --_moduleIterator;

    return (*this);
}

template<class DataType, class NestedIteratorType>
CTL::SingleViewData::PixelIterator<DataType, NestedIteratorType>
SingleViewData::PixelIterator<DataType, NestedIteratorType>::operator--(int)
{
    auto temp(*this);
    --(*this);
    return temp;
}

template<class DataType, class NestedIteratorType>
uint SingleViewData::PixelIterator<DataType, NestedIteratorType>::module() const
{
    return _curModule;
}

template<class DataType, class NestedIteratorType>
uint SingleViewData::PixelIterator<DataType, NestedIteratorType>::pixel() const
{
    return std::distance(_dataPtr->module(_curModule).data().begin(), _moduleIterator);
}

template<class DataType, class NestedIteratorType>
void SingleViewData::PixelIterator<DataType, NestedIteratorType>::nextModule()
{
    ++_curModule;

    if(_curModule < _dataPtr->nbModules())
        _moduleIterator = _dataPtr->module(_curModule).data().begin();
    else
        --_curModule;
}

template<class DataType, class NestedIteratorType>
void SingleViewData::PixelIterator<DataType, NestedIteratorType>::prevModule()
{
    --_curModule;

    if(_curModule >= 0)
        _moduleIterator = _dataPtr->module(_curModule).data().end();
    else
        ++_curModule;

}


} // namespace CTL

/*! \file */

#endif // CTL_SINGLEVIEWDATA_H
