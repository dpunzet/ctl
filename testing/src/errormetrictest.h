#ifndef ERRORMETRICTEST_H
#define ERRORMETRICTEST_H

#include <QtTest>

class ErrorMetricTest : public QObject
{
    Q_OBJECT
public:
    ErrorMetricTest() = default;

private Q_SLOTS:
    void initTestCase();
    void testCorr() const;
    void testLN() const;
    void testGMC() const;
    void testRMSE() const;

private:
    std::vector<float> _randSeq1;
    std::vector<float> _randSeq2;
};

#endif // ERRORMETRICTEST_H
